@extends('layouts.theme.master')

@section('content')
	<section id="root">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						<Products :products="{{ json_encode($products) }}"></Products>
					</div><!--features_items-->
					
				</div>
			</div>
		</div>
	</section>
@endsection