@component('dashboard::components.table-box')

        @slot('title', trans('customers.favorites.list'))

        <thead>
        <tr>
            <th>@lang('doctors.attributes.name')</th>
            <th>@lang('customers.favorites.created_at')</th>
        </tr>
        </thead>
        <tbody>
        @forelse($customer->getFavorites() as $favorite)
            <tr>
                <td>
                    <a href="{{ 
                        route('dashboard.'. $favorite->favorite->getType() . 's' .'.show', $favorite->favorite) 
                    }}" class="text-decoration-none text-ellipsis">
                            <span class="index-flag">
                            @include('dashboard.accounts.customers.partials.flags.svg')
                            </span>
                        <img src="{{ $favorite->favorite->getAvatar() }}"
                             alt="Product 1"
                             class="img-circle img-size-32 mr-2">
                        {{ $favorite->favorite->name }}
                    </a>
                </td>

                
                <td>{{ $customer->created_at->format('Y-m-d') }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="100" class="text-center">@lang('customers.favorites.empty')</td>
            </tr>
        @endforelse
    @endcomponent
