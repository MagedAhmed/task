@component('dashboard::components.table-box')
    @slot('title', trans('reservations.actions.list'))

    <thead>
    <tr>
        <th class="d-none d-md-table-cell">@lang('reservations.attributes.doctor')</th>
        <th>@lang('reservations.attributes.client')</th>
        <th>@lang('reservations.attributes.status')</th>
        <th>@lang('reservations.attributes.workingDay')</th>
        <th style="width: 160px">...</th>
    </tr>
    </thead>
    <tbody>
    @forelse($reservations as $reservation)
        <tr>
            <td class="d-none d-md-table-cell">
                {{ $reservation->doctor->name }}
            </td>
            <td>
                {{ $reservation->client->name }}
            </td>
            <td>
                <i class="fa fa-lg 
                    @if($reservation->status == 1) fa-clock text-gray
                    @elseif($reservation->status == 2) fa-ban text-red
                    @else fa-check text-green @endif">
                </i>
            </td>
            <td>
                {{ $reservation->workingDay->day }}
            </td>
            <td style="width: 160px">
                @include('dashboard.reservations.partials.actions.show')
                @include('dashboard.reservations.partials.actions.edit')
                @include('dashboard.reservations.partials.actions.delete')
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="100" class="text-center">@lang('reservations.empty')</td>
        </tr>
    @endforelse

    @if($reservations->hasPages())
        @slot('footer')
            {{ $reservations->links() }}
        @endslot
    @endif
@endcomponent