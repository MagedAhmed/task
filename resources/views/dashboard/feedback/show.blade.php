<x-layout :title="$feedback->id" :breadcrumbs="['dashboard.feedback.show', $feedback]">
    <div class="row">
        <div class="col-md-12">
            @component('dashboard::components.box')
                @slot('class', 'p-0')
                @slot('bodyClass', 'p-0')

                <table class="table table-striped table-middle">
                    <tbody>
                    <tr>
                        <th>@lang('feedback.attributes.title')</th>
                        <td>{{ $feedback->title }}</td>
                    </tr>
                    <tr>
                        <th>@lang('feedback.attributes.message')</th>
                        <td>{{ $feedback->message }}</td>
                    </tr>
                    <tr>
                        <th>@lang('feedback.attributes.created_at')</th>
                        <td>{{ $feedback->created_at->toDateTimeString() }} ({{ $feedback->created_at->diffForHumans() }})</td>
                    </tr>
                    </tbody>
                </table>

                @slot('footer')
                    @include('dashboard.feedback.partials.actions.delete')
                @endslot
            @endcomponent

        </div>
    </div>
</x-layout>