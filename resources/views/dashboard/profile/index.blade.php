<x-layout :title="trans('profiles.plural')" :breadcrumbs="['dashboard.profiles.index']">
{{ BsForm::resource($resourceType)->put(route('dashboard.profile.update', $user->id)) }}
    @component('dashboard::components.box')

        {{ BsForm::text('name')->value($user->name) }}
        {{ BsForm::text('email')->value($user->email) }}
        {{ BsForm::text('phone')->value($user->phone) }}
        {{ BsForm::password('password') }}
        {{ BsForm::password('password_confirmation') }}

        {{ BsForm::image('avatar')->collection('avatars')->files($user->getMediaResource('avatars')) }}

        {{ BsForm::select('show_email')->options([
            '0' => trans('profiles.attributes.hide'), 
            '1' => trans('profiles.attributes.show'), 
        ])->value($user->show_email) }}

        {{ BsForm::select('show_phone')->options([
            '0' => trans('profiles.attributes.hide'), 
            '1' => trans('profiles.attributes.show'), 
        ])->value($user->show_phone) }}

        {{ BsForm::select('active_chat')->options([
            '0' => trans('profiles.attributes.inactive'), 
            '1' => trans('profiles.attributes.active'), 
        ])->value($user->active_chat) }}

        @slot('footer')
            {{ BsForm::submit()->label(trans('admins.actions.save')) }}
        @endslot
    @endcomponent
{{ BsForm::close() }}
</x-layout>

@prepend('scripts')
<script>
    $(function() {
        /** Get Cities related to the chosen country  */
        $('select[name=country_id]').change(function() {
            $.ajax({
                url: "/api/cities",
                data: {
                    country_id: $(this).val()
                },
                success: function(data) {
                    var select = $('form select[name= city_id]');
                    $('.cities').removeClass('d-none');
                    select.empty();

                    $.each(data,function(key, value) {
                        select.append('<option value=' + key + '>' + value + '</option>');
                    });
                }
            });
        });
        
        /** Get Areas related to the chosen city  */
        $('select[name=city_id]').change(function() {
            $.ajax({
                url: "/api/areas",
                data: {
                    city_id: $(this).val()
                },
                success: function(data) {
                    var select = $('form select[name= area_id]');
                    $('.areas').removeClass('d-none');
                    select.empty();

                    $.each(data,function(key, value) {
                        select.append('<option value=' + key + '>' + value + '</option>');
                    });
                }
            });
        });


    });
</script>
@endprepend
