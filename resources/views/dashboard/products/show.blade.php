<x-layout :title="$product->name" :breadcrumbs="['dashboard.products.show', $product]">
    <div class="row">
        <div class="col-md-12">
            @component('dashboard::components.box')
                @slot('class', 'p-0')
                @slot('bodyClass', 'p-0')

                <table class="table table-striped table-middle">
                    <tbody>
                    <tr>
                        <th width="200">@lang('products.attributes.name')</th>
                        <td>{{ $product->name }}</td>
                    </tr>
                    <tr>
                        <th width="200">@lang('products.attributes.description')</th>
                        <td>{!! $product->description !!}</td>
                    </tr>
                    <tr>
                        <th width="200">@lang('products.attributes.price')</th>
                        <td>{{ $product->price }}</td>
                    </tr>
                    <tr>
                        <th width="200">@lang('products.attributes.main_image')</th>
                        <td>
                            <img src="{{ $product->getMainImage() }}"
                                style="max-width:400px;"
                                alt="{{ $product->name }}">
                         </td>
                    </tr>
                    </tbody>
                </table>

                @slot('footer')
                    @include('dashboard.products.partials.actions.edit')
                    @include('dashboard.products.partials.actions.delete')
                    @include('dashboard.products.partials.actions.images')
                @endslot
            @endcomponent

        </div>
        
    </div>
</x-layout>
