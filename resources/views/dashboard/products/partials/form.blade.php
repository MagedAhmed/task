@include('dashboard.errors')
{{ BsForm::text('name') }}
{{ BsForm::textarea('description')
->attribute('class', 'form-control editor') }}
{{ BsForm::number('price') }}

{{ BsForm::image('main_image')->collection('main_image')->files($product->getMediaResource('main_image')) }}

@push('scripts')
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
      tinymce.init({
        selector: '.editor'
      });
    </script>
    
@endpush
