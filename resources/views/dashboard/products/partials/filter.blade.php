{{ BsForm::resource('products')->get(url()->current()) }}
@component('dashboard::components.box')
    @slot('title', trans('products.search'))

    <div class="row">
        <div class="col-md-4">
            {{ BsForm::text('name')->value(request('name')) }}
        </div>
        <div class="col-md-4">
            {{ BsForm::text('description')->value(request('description')) }}
        </div>
        <div class="col-md-4">
            {{ BsForm::number('perPage')
                ->value(request('perPage', 15))
                ->min(1)
                 ->label(trans('products.perPage')) }}
        </div>
    </div>

    @slot('footer')
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fas fa fa-fw fa-filter"></i>
            @lang('products.actions.search')
        </button>
    @endslot
@endcomponent
{{ BsForm::close() }}
