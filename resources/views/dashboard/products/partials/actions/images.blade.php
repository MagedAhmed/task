@can('update', $product)
    <a href="{{ route('dashboard.products.images', $product) }}" class="btn btn-outline-info btn-sm">
        <i class="fas fa fa-fw fa-image"></i>
    </a>
@endcan
