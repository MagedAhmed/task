<x-layout :title="$product->name" :breadcrumbs="['dashboard.products.edit', $product]">
    {{ BsForm::put(route('dashboard.products.update.images', $product)) }}
    @component('dashboard::components.box')
        @slot('title', trans('products.actions.edit-images'))
        @include('dashboard.errors')

        {{ BsForm::image('images')->max(10)->collection('images')->files($product->getMediaResource('images')) }}
        
        @slot('footer')
            {{ BsForm::submit()->label(trans('products.actions.save')) }}
        @endslot
    @endcomponent
    {{ BsForm::close() }}
</x-layout>

