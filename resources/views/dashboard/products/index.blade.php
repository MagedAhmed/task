<x-layout :title="trans('products.plural')" :breadcrumbs="['dashboard.products.index']">
    @include('dashboard.products.partials.filter')

    @component('dashboard::components.table-box')
        @slot('title', trans('products.actions.list'))
        @slot('tools')
            @include('dashboard.products.partials.actions.create')
        @endslot

        <thead>
        <tr>
            <th>@lang('products.attributes.name')</th>
            <th class="d-none d-md-table-cell">@lang('products.attributes.description')</th>
            <th class="d-none d-md-table-cell">@lang('products.attributes.price')</th>
            <th style="width: 160px">...</th>
        </tr>
        </thead>
        <tbody>
        @forelse($products as $product)
            <tr>
                <td>
                    <a href="{{ route('dashboard.products.show', $product) }}"
                       class="text-decoration-none text-ellipsis">
                        {{ $product->name }}
                    </a>
                </td>
                <td class="d-none d-md-table-cell">
                    {!! $product->description !!}
                </td>
                <td class="d-none d-md-table-cell">
                    {{ $product->price }}
                </td>
                <td style="width: 200px">
                    @include('dashboard.products.partials.actions.show')
                    @include('dashboard.products.partials.actions.images')
                    @include('dashboard.products.partials.actions.edit')
                    @include('dashboard.products.partials.actions.delete')
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="100" class="text-center">@lang('products.empty')</td>
            </tr>
        @endforelse

        @if($products->hasPages())
            @slot('footer')
                {{ $products->links() }}
            @endslot
        @endif
    @endcomponent
</x-layout>
