@component('dashboard::components.sidebarItem')
    @slot('url', route('dashboard.home'))
    @slot('name', trans('dashboard.home'))
    @slot('icon', 'fas fa-tachometer-alt')
    @slot('active', request()->routeIs('dashboard.home'))
@endcomponent

@include('dashboard.accounts.sidebar')

@include('dashboard.products.partials.actions.sidebar')

@component('dashboard::components.sidebarItem')
    @slot('url', route('dashboard.profile.index'))
    @slot('name', trans('profiles.plural'))
    @slot('icon', 'fas fa-user')
    @slot('can', ['ability' => 'notAdmin', 'model' => \App\Models\User::class])
    @slot('active', request()->routeIs('dashboard.profile.index'))
@endcomponent

@component('dashboard::components.sidebarItem')
    @slot('url', route('dashboard.feedback.index'))
    @slot('name', trans('feedback.plural'))
    @slot('icon', 'fas fa-envelope')
    @slot('can', ['ability' => 'viewAny', 'model' => \App\Models\Feedback::class])
    @slot('active', request()->routeIs('dashboard.feedback.index'))
@endcomponent

@component('dashboard::components.sidebarItem')
    @slot('url', route('dashboard.settings.index'))
    @slot('name', trans('settings.plural'))
    @slot('icon', 'fas fa-cog')
    @slot('can', ['ability' => 'viewAny', 'model' => \App\Models\Setting::class])
    @slot('active', request()->routeIs('dashboard.settings.index'))
@endcomponent
