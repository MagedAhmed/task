@extends('layouts.theme.master')

@section('content')
<main>
    <div class="p-10 container mx-auto mt-10">
        <!--main photo-->
        @if($blog->getFirstMediaUrl('feature_images')) 
        <div class="flex relative mx-auto" style="height:450px; width: 1000px;">
            <div class="bg-gray-200 rounded-lg w-full h-full"></div>
            <img class="w-full h-full px-12 rounded-lg mt-12 absolute left-4" src="{{ $blog->getFirstMediaUrl('feature_images') }}" alt="Feature image">
        </div>
        @endif
        <!--project details-->
        <div class="w-5/6 ml-32 mt-32"> 
            <!--blog header-->
            <p class="text-xl text-black opacity-75 mb-24"> 
            {!! $blog->description !!}
            </p>
        </div>
    </div>
</main>
    
@endsection