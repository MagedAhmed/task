<?php

return [
    'plural' => 'المشاريع',
    'singular' => 'المشروع',
    'empty' => 'لا توجد مشاريع',
    'select' => 'اختر المشروع',
    'select-type' => 'الكل',
    'perPage' => 'عدد النتائج في الصفحة',
    'filter' => 'ابحث عن مشروع',
    'actions' => [
        'list' => 'عرض الكل ',
        'show' => 'عرض',
        'create' => 'إضافة مشروع جديد',
        'edit' => 'تعديل  المشروع',
        'delete' => 'حذف المشروع',
        'save' => 'حفظ',
        'filter' => 'بحث',
    ],
    'messages' => [
        'created' => 'تم إضافة المشروع بنجاح .',
        'updated' => 'تم تعديل المشروع بنجاح .',
        'deleted' => 'تم حذف المشروع بنجاح .',
    ],
    'attributes' => [
        'title' => 'اسم المشروع',
        'description' => 'وصف المشروع',
        'url' => 'الرابط',
        'github' => 'github',
        'image' => 'صور المشروع',
    ],
    'default' => [
        'name' => 'اسم المشروع',
    ],
    'dialogs' => [
        'delete' => [
            'title' => 'تحذير !',
            'info' => 'هل أنت متأكد انك تريد حذف هذا المشروع ?',
            'confirm' => 'حذف',
            'cancel' => 'إلغاء',
        ],
    ],

];