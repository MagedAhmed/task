<?php

return [
    'plural' => 'الملف الشخصي',
    'attributes' => [
        'show' => 'اظهار',
        'hide' => 'اخفاء',
        'active' => 'تفعيل',
        'inactive' => 'غير مفعل',
    ],
];
