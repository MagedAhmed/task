<?php

return [
    'singular' => 'Blog',
    'plural' => 'Blogs',
    'empty' => 'There are no blogs yet.',
    'count' => 'Blogs count',
    'search' => 'Search',
    'blog' => 'Blog',
    'actions' => [
        'list' => 'List all',
        'create' => 'Create Blog',
        'show' => 'Show Blog',
        'edit' => 'Edit Blog',
        'delete' => 'Delete Blog',
        'options' => 'Options',
        'save' => 'Save',
        'close' => 'Close',
        'share' => 'Share Blog',
    ],
    'messages' => [
        'created' => 'The blog has been created successfully.',
        'updated' => 'The blog has been updated successfully.',
        'deleted' => 'The blog has been deleted successfully.',
    ],
    'attributes' => [
        'name' => 'Blog Name',
        'description' => 'Description',
        'image' => 'Image',
        'created_at' => 'created At',
    ],
    'dialogs' => [
        'delete' => [
            'title' => 'Warning !',
            'info' => 'Are you sure you want to delete the blog ?',
            'confirm' => 'Delete',
            'cancel' => 'Cancel',
        ],
    ],
];