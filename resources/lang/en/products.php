<?php

return [
    'singular' => 'Product',
    'plural' => 'Products',
    'empty' => 'There are no Products yet.',
    'count' => 'Products count',
    'search' => 'Search for Product',
    'perPage' => 'Count Results Per Page',
    'actions' => [
        'list' => 'List all',
        'create' => 'Create Product',
        'show' => 'Show',
        'edit' => 'Edit',
        'edit' => 'Edit Products',
        'delete' => 'Delete Product',
        'options' => 'Options',
        'save' => 'Save',
        'search' => 'Search',
    ],
    'messages' => [
        'created' => 'The Product has been created successfully.',
        'updated' => 'The Product has been updated successfully.',
        'deleted' => 'The Product has been deleted successfully.',
    ],
    'attributes' => [
        'name' => 'Product name',
        'description' => 'Product description',
        'images' => 'Images',
        'main_image' => 'Main image',
        'price' => 'Price',
        'media' => 'Main Image'
    ],
    'dialogs' => [
        'delete' => [
            'title' => 'Warning !',
            'info' => 'Are you sure you want to delete the Product ?',
            'confirm' => 'Delete',
            'cancel' => 'Cancel',
        ],
    ],
];