<?php

return [
    /*
     * The layout of dashboard.
     *
     * Supported : "vali", "adminlte"
     */
    'dashboard' => 'adminlte',

    /*
     * The layout of front end.
     *
     * Supported : "vali", "adminlte"
     */
    'frontend' => 'adminlte',
];
