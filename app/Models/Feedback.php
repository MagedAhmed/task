<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Feedback extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'message',
    ];

    /**
     * Mark message as readed.
     *
     * @return $this
     */
    public function markAsRead()
    {
        $this->forceFill(['readed_at' => now()])->save();

        return $this->refresh();
    }

    /**
     * Determine whether the message is readed.
     *
     * @return bool
     */
    public function isReaded()
    {
        return ! ! $this->getAttribute('readed_at');
    }
}
