<?php

namespace App\Models;

use AhmedAliraqi\LaravelMediaUploader\Entities\Concerns\HasUploader;
use App\Http\Filters\Filterable;
use App\Http\Filters\ProductFilter;
use App\Http\Resources\ProductResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Product extends Model implements HasMedia
{
    use HasFactory,
        Notifiable,
        InteractsWithMedia,
        Filterable,
        HasUploader;


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'mainImage'
    ];

    protected $guarded = [];
    /**
     * The model filter name.
     *
     * @var string
     */
    protected $filter = ProductFilter::class;

    /**
     * @return \App\Http\Resources\CustomerResource
     */
    public function getResource()
    {
        return new ProductResource($this);
    }   
    
    /**
     * The product main image url.
     *
     * @return bool
     */
    public function getMainImage()
    {
        return $this->getFirstMediaUrl('main_image');
    }
    

    public function getMainImageAttribute() {
        return $this->getFirstMediaUrl('main_image');
    }
}
