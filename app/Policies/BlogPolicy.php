<?php

namespace App\Policies;

use App\Models\Like;
use App\Models\User;
use App\Models\Blog;
use Illuminate\Auth\Access\HandlesAuthorization;

class BlogPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any the blogs.
     *
     * @param  \App\Models\User|null $user
     * @return mixed
     */
    public function viewAny(?User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the blog.
     *
     * @param  \App\Models\User|null $user
     * @param  \App\Models\Blog $blog
     * @return mixed
     */
    public function view(?User $user, Blog $blog)
    {
        return true;
    }

    /**
     * Determine whether the user can create blogs.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the blog.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Blog $blog
     * @return mixed
     */
    public function update(User $user, Blog $blog)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the blog.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Blog $blog
     * @return mixed
     */
    public function delete(User $user, Blog $blog)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can comment on the blog.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Blog $blog
     * @return mixed
     */
    public function comment(User $user, Blog $blog)
    {
        return true;
    }

    /**
     * Determine whether the user can like the blog.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Blog $blog
     * @return mixed
     */
    public function like(User $user, Blog $blog)
    {
        return ! Like::where('user_id', $user->id)
            ->where('likable_type', Blog::class)
            ->where('likable_id', $blog->id)
            ->exists();
    }

    /**
     * Determine whether the user can unlike the blog.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Blog $blog
     * @return mixed
     */
    public function unlike(User $user, Blog $blog)
    {
        return Like::where('user_id', $user->id)
            ->where('likable_type', Blog::class)
            ->where('likable_id', $blog->id)
            ->exists();
    }

    /**
     * Determine whether the user can restore the blog.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Blog $blog
     * @return mixed
     */
    public function restore(User $user, Blog $blog)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the blog.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Blog $blog
     * @return mixed
     */
    public function forceDelete(User $user, Blog $blog)
    {
        return $user->isAdmin();
    }
}
