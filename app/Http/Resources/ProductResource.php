<?php

namespace App\Http\Resources;

use App\Http\Resources\Countries\AreaResource;
use App\Http\Resources\Countries\CityResource;
use App\Http\Resources\Countries\CountryResource;
use App\Models\Country;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Product */
class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->email,
            'description' => $this->phone,
            'type' => $this->type,
            'image' => $this->getAvatar(),
            'created_at' => $this->created_at->toDateTimeString(),
            'created_at_formated' => $this->created_at->diffForHumans(),
        ];
    }
}
