<?php

namespace App\Http\Controllers\Feedbacks\Api;

use Notification;
use App\Models\Admin;
use App\Models\Feedback;
use App\Events\FeedbackSent;
use App\Http\Controllers\Controller;
use App\Notifications\FeedbackNotification;
use App\Http\Requests\Feedbacks\FeedbackRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class FeedbackController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * @param FeedbackRequest $request
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FeedbackRequest $request)
    {
        $this->authorize('create', Feedback::class);

        // $feedback = Feedback::create($request->all());

        // Notification::send(Admin::all(), new FeedbackNotification());

        event(new FeedbackSent(Feedback::create($request->all())));

        return response()->json([
            'message' => trans('feedback.messages.sent'),
        ]);
    }
}
