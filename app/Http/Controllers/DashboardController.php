<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\Product;
use Illuminate\Routing\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedbackCount = $this->getFeedback();
        $productCount = $this->getProduct();

        return view('dashboard.home', get_defined_vars());
    }

    /**
     * Get Product count.
     *
     * @return int
     */
    public function getProduct()
    {
        return Product::count();
    }

    /**
     * Get Feedback count.
     *
     * @return int
     */
    public function getFeedback()
    {
        return Feedback::count();
    }
}
