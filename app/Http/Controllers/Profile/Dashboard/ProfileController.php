<?php

namespace App\Http\Controllers\Profile\Dashboard;

use App\Models\Area;
use App\Models\City;
use App\Models\User;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ProfileController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * Display a listing of the resource.
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('update', auth()->user());

        $resourceType = $this->getResourceType();
        $user = auth()->user();

        return view("dashboard.profile.index", compact('user', 'resourceType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProfileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request)
    {
        $this->authorize('update', auth()->user());

        auth()->user()->update($request->allWithHashedPassword());

        auth()->user()->addAllMediaFromTokens();

        flash(trans('customers.messages.updated'));

        return back();
    }

    /**
     * Get proper Resource Type.
     *
     * @return void
     */
    public function getResourceType()
    {
        return auth()->user()->getType();
    }
}
