<?php

namespace App\Http\Controllers\Products\Dashboard;

use App\Events\ProductCreated;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->authorizeResource(Product::class, 'product');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::filter()->paginate();

        return view('dashboard.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('dashboard.products.create')
            ->withProduct(new Product());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Accounts\ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price
        ]);

        $product->addAllMediaFromTokens();

        event(new ProductCreated($product));
        
        flash(trans('products.messages.created'));

        return redirect()->route('dashboard.products.show', $product);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('dashboard.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('dashboard.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Accounts\ProductRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductRequest $request, Product $product)
    {
        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price
        ]);

        $product->addAllMediaFromTokens();

        flash(trans('products.messages.updated'));

        return redirect()->route('dashboard.products.show', $product);
    }
    
    /**
     * Show Images
     *
     * @param  App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function showImages(Product $product)
    {
        return view('dashboard.products.partials.images', compact('product'));
    }
    
    /**
     * Update model Images
     *
     * @param  App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function updateImages(Product $product)
    {
        $product->addAllMediaFromTokens();

        flash(trans('products.messages.updated'));

        return redirect()->route('dashboard.products.show', $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Product $product)
    {
        $product->delete();

        flash(trans('products.messages.deleted'));

        return redirect()->route('dashboard.products.index');
    }
}
