<?php

namespace App\Http\Controllers\Products\Api;

use App\Http\Requests\Accounts\Api\CompleteProfileRequest;
use App\Models\User;
use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\Accounts\Api\ProfileRequest;
use App\Models\Product;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ProductController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    public function index()
    {
        $products = Product::latest()->get();

        return json_encode($products);
    }
}
