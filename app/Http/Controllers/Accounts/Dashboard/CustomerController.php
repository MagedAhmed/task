<?php

namespace App\Http\Controllers\Accounts\Dashboard;

use App\Models\Area;
use App\Models\City;
use App\Models\Doctor;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Reservation;
use Illuminate\Routing\Controller;
use App\Http\Requests\Accounts\CustomerRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CustomerController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->authorizeResource(Customer::class, 'customer');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isAdmin()) {
            //admin
            $customers = Customer::filter()->paginate();
        } elseif (auth()->user()->hasDoctors()) {
            //hospital
            $customers = Customer::whereHas('reservations', function ($query) {
                $query->whereHas('doctor', function ($doctor) {
                    $doctor->where('owner_id', auth()->id());
                });
            })->filter()->paginate();
        } else {
            // doctor
            $customers = Customer::whereHas('reservations', function ($query) {
                $query->where('doctor_id', auth()->id());
            })->filter()->paginate();
        }


        return view('dashboard.accounts.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all()->pluck('name', 'id');
        $cities = City::all()->pluck('name', 'id');
        $areas = Area::all()->pluck('name', 'id');

        return view('dashboard.accounts.customers.create', compact('countries', 'cities', 'areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Accounts\CustomerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CustomerRequest $request)
    {
        $customer = Customer::create($request->allWithHashedPassword());

        $customer->setType($request->type);

        $customer->addAllMediaFromTokens();

        flash(trans('customers.messages.created'));

        return redirect()->route('dashboard.customers.show', $customer);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        $reservations = $customer->reservations()->filter()->paginate();

        return view('dashboard.accounts.customers.show', compact('customer', 'reservations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $countries = Country::all()->pluck('name', 'id');
        $cities = City::all()->pluck('name', 'id');
        $areas = Area::all()->pluck('name', 'id');

        return view('dashboard.accounts.customers.edit', compact('customer', 'countries', 'cities', 'areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Accounts\CustomerRequest $request
     * @param \App\Models\Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CustomerRequest $request, Customer $customer)
    {
        $customer->update($request->allWithHashedPassword());

        $customer->setType($request->type);

        $customer->addAllMediaFromTokens();

        flash(trans('customers.messages.updated'));

        return redirect()->route('dashboard.customers.show', $customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Customer $customer
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();

        flash(trans('customers.messages.deleted'));

        return redirect()->route('dashboard.customers.index');
    }
}
