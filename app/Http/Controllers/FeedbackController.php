<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function store(Request $request)
    {
        Feedback::create($request->all());
        
        flash(trans('feedback.created'));
        
        return back();
    }
}
