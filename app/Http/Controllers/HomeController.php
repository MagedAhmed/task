<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Response;

class HomeController extends Controller
{
    public function home()
    {
        $products = Product::latest()->get();
        
        return view('welcome', compact('products'));
    }
}
