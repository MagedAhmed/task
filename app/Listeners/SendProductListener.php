<?php

namespace App\Listeners;

use App\Events\ProductCreated;
use App\Models\Admin;
use App\Notifications\ProductNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendProductListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductCreated  $event
     * @return void
     */
    public function handle(ProductCreated $event)
    {
        Admin::chunk(10, function ($admins) use ($event) {
            Notification::send(
                $admins,
                new ProductNotification($event->product)
            );
        });
    }
}
