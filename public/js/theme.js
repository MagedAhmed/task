	

	//Toggle mode
	const toggle = document.querySelector('.js-change-theme');
	const backimg = document.querySelector('.bg-cover');
	const body = document.querySelector('body');
	const profile = document.querySelector('.toggle');
	const inverse = document.querySelector('.inverse-toggle');
	
	toggle.addEventListener('click', () => {

		if (body.classList.contains('text-gray-800')) {
			toggle.innerHTML = "☀️";
			body.classList.remove('text-gray-800');
			body.classList.add('text-gray-100');
			body.classList.add('bg-gray-800');
			profile.classList.remove('bg-white');
			profile.classList.add('bg-gray-800');
			inverse.classList.add('bg-white');
			inverse.classList.remove('bg-gray-800');
			inverse.classList.remove('text-gray-100');
			inverse.classList.add('text-gray-800');
			backimg.style.removeProperty('background-image');
		} else
		{
			toggle.innerHTML = "🌙";
			body.classList.remove('text-gray-100');
			body.classList.remove('bg-gray-800');
			body.classList.add('text-gray-800');
			profile.classList.remove('bg-gray-800');			
			profile.classList.add('bg-white');
			inverse.classList.add('bg-gray-800');			
			inverse.classList.remove('bg-white');
			backimg.classList.remove('bg-transparent');
			backimg.style.backgroundImage = "url('/img/bg.svg')";
		}
	});





	// modal

	var openmodal = document.querySelectorAll('.modal-open')
    for (var i = 0; i < openmodal.length; i++) {
      openmodal[i].addEventListener('click', function(event){
    	event.preventDefault()
    	toggleModal()
      })
    }
    
    const overlay = document.querySelector('.modal-overlay')
    overlay.addEventListener('click', toggleModal)
    
    var closemodal = document.querySelectorAll('.modal-close')
    for (var i = 0; i < closemodal.length; i++) {
      closemodal[i].addEventListener('click', toggleModal)
    }
    
    document.onkeydown = function(evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
    	isEscape = (evt.key === "Escape" || evt.key === "Esc")
      } else {
    	isEscape = (evt.keyCode === 27)
      }
      if (isEscape && document.body.classList.contains('modal-active')) {
    	toggleModal()
      }
    };
    
    
    function toggleModal () {
      const body = document.querySelector('body')
      const modal = document.querySelector('.modal')
      modal.classList.toggle('opacity-0')
      modal.classList.toggle('pointer-events-none')
      body.classList.toggle('modal-active')
    }
	
	
	document.getElementsByClassName('cv').addEventListener('click', () => {
		alert("ok");
	});