<?php

namespace Tests\Feature\Feedbacks\Dashboard;

use Tests\TestCase;
use App\Models\Feedback;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeedbackTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_list_feedbacks()
    {
        $this->actingAsAdmin();

        $feedback = Feedback::factory()->create();

        $response = $this->get(route('dashboard.feedback.index'));

        $response->assertSuccessful();

        $response->assertSee($feedback->title);
    }

    /** @test */
    public function it_can_display_feedback_details()
    {
        $this->actingAsAdmin();

        $feedback = Feedback::factory()->create();

        $response = $this->get(route('dashboard.feedback.show', $feedback));

        $response->assertSuccessful();

        $response->assertSee(e($feedback->message));
    }

    /** @test */
    public function it_can_delete_feedback()
    {
        $this->actingAsAdmin();

        $feedback = Feedback::factory()->create();

        $feedbackCount = Feedback::count();

        $response = $this->delete(route('dashboard.feedback.destroy', $feedback));
        $response->assertRedirect();

        $this->assertEquals(Feedback::count(), $feedbackCount - 1);
    }
}
