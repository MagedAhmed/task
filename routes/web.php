<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('dashboard.locales')->group(function () {
    Auth::routes();
});

Route::get('/', [HomeController::class, 'home']);
Route::post('/feedback', [FeedbackController::class, 'store']);
Route::get('/blog/{blog}', [BlogController::class, 'show'])->name('blog.show');
