<?php

// Home / settings
Breadcrumbs::register('dashboard.profiles.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.home');
    $breadcrumbs->push(trans('profiles.plural'), route('dashboard.profile.index'));
});
